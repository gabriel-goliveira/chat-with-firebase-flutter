import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';




//Login Google
final googleSignIn = GoogleSignIn();
//Singleton, uma instância só
final auth = FirebaseAuth.instance ;


//Verificar se está logado ou efetuar login
//Autenticar no google
Future<Null>_ensureLoggedIn() async {
  GoogleSignInAccount user = googleSignIn.currentUser;
  //Se esse usuario for nulo
  if(user==null)
    //Login Silencioso, não irá aparecer mensagem
  user =  await googleSignIn.signInSilently();
  //Se o login silencioso não funcionar, executar login padrão
  if(user==null)
    user = await googleSignIn.signIn();
  //Autenticar no Firebase com o usuário do google
  //Verificar se o usuário está logado no FireBase agora
  if(await auth.currentUser() == null){
    GoogleSignInAuthentication credentials = await googleSignIn.currentUser.authentication;
    await auth.signInWithGoogle(idToken: credentials.idToken, accessToken: credentials.accessToken);
  }
}

_handleSubmitted(String text) async{
  await _ensureLoggedIn();
  _sendMessage(text: text);


}

void _sendMessage({String text, String imgUrl}){
  Firestore.instance.collection("messages").add(
    {
      "text" : text,
      "imgUrl" : imgUrl,
      "senderName" : googleSignIn.currentUser.displayName,
      "senderPhotoUrl" : googleSignIn.currentUser.photoUrl,
    }
  );
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      top: false,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Chat App"),
          centerTitle: true,
          elevation:
              Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
        ),
        body: Column(
          children: <Widget>[
            Expanded(
            child: StreamBuilder(
              stream: Firestore.instance.collection("messages").snapshots(),
                builder: (context, snapshot){
                  switch(snapshot.connectionState){
                    case ConnectionState.none:
                    case ConnectionState.waiting:
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    default:
                      return ListView.builder(
                        reverse: true,
                          itemCount: snapshot.data.documents.length,
                          itemBuilder: (context, index){
                          List r = snapshot.data.documents.reversed.toList();
                          return ChatMessage(r[index].data);
                          }
                      );
                  }
                }
            ),
        ),
            Divider(
              height: 1.0,
            ),
            Container(
              decoration: BoxDecoration(
                color: Theme.of(context).cardColor,
              ),
              child: TextComposer(),
            ),
          ],
        ),
      ),
    );
  }
}


//Área onde será inserido o texto das mensagens
class TextComposer extends StatefulWidget {
  @override
  _TextComposerState createState() => _TextComposerState();
}

class _TextComposerState extends State<TextComposer> {
  bool _isComposing = false;
  final _textController = TextEditingController();

  void _reset(){
    _textController.clear();
    setState(() {
      _isComposing = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return IconTheme(
      data: IconThemeData(color: Theme.of(context).accentColor),
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 8.0),
        decoration: Theme.of(context).platform == TargetPlatform.iOS
            ? BoxDecoration(
                border: Border(top: BorderSide(color: Colors.grey[200])),
              )
            : null,
        child: Row(
          children: <Widget>[
            Container(
              child: IconButton(icon: Icon(Icons.camera), onPressed: () async{
                await _ensureLoggedIn();
                File imgFile = await  ImagePicker.pickImage(source: ImageSource.camera);
                if(imgFile == null) return;
                //Pegamos a referencia do Storage
                StorageUploadTask task = FirebaseStorage.instance.ref().
                //Dentro do child passamos o nome, que aqui é a junção do id e do tempo e por fim adicionamos o arquivo
                child(googleSignIn.currentUser.id.toString() +
                    DateTime.now().millisecondsSinceEpoch.toString()).putFile(imgFile);
                //tasksnapshot recebe a referencia da task(nome) assim que completo o upload
                StorageTaskSnapshot taskSnapshot = await task.onComplete;
                //url do download da imagem
                //Pego da tasksnapshot
                String url = await taskSnapshot.ref.getDownloadURL();
                //envia a imagem com a url do download
                _sendMessage(imgUrl: url);
              }),
            ),
            Expanded(
              child: TextField(
                controller: _textController,
                decoration:
                    InputDecoration.collapsed(hintText: "Enviar uma Mensagem"),
                onChanged: (text) {
                  setState(() {
                    _isComposing = text.length > 0;
                  });
                },
                onSubmitted: (text){
                  _handleSubmitted(text);
                  _reset();
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 4.0),
              //Se for IOS, botão cupertino senão icon button
              child: Theme.of(context).platform == TargetPlatform.iOS
                  ? CupertinoButton(
                      child: Text("ENVIAR"),
                      onPressed: _isComposing
                          ? () {
                        _handleSubmitted(_textController.text);
                        _reset();
                      }
                          : null //se não estiver compondo, ele nao vai ficar azul, o botão vai ficar desabilitado,
                      )
                  : IconButton(
                      icon: Icon(Icons.send),
                      onPressed: _isComposing ? () {
                        _handleSubmitted(_textController.text);
                        _reset();
                      } : null),
            ),
          ],
        ),
      ),
    );
  }
}

//Balões das Mensagens
class ChatMessage extends StatelessWidget {

  final Map<String, dynamic> data;
  ChatMessage(this.data);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 16.0),
            child: CircleAvatar(
              backgroundImage: NetworkImage(data["senderPhotoUrl"]),
            ),
          ),
          Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                     data["senderName"],
                    style: Theme.of(context).textTheme.subhead,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5.0),
                    child: data["imgUrl"] != null ?
                    Image.network(data["imgUrl"], width: 250.0,) : 
                    Text(data["text"]),
                  ),
                ],
              ),
          ),
        ],
      ),
    );
  }
}

