import 'package:chat_online/view/home.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';


void main(){
  runApp(MyApp());
}



class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Chat App",
      home: Home(),
      theme: Theme.of(context).platform == TargetPlatform.iOS ?
      kIOSTheme : kDefault,

    );
  }
}


final ThemeData kIOSTheme = ThemeData(
  primarySwatch: Colors.orange,
  primaryColor: Colors.grey[100],
  primaryColorBrightness: Brightness.light,
);

final ThemeData kDefault = ThemeData(
  primaryColor: Colors.purple,
  accentColor: Colors.orangeAccent[400],
);